package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 修改营销活动状态请求值
 */
public class UpdatePromotionActivityStatusV2Query extends BaseQuery {


    /**
     * <p>营销活动ID</p>
     */
    private String activity_id;
    /**
     * <p>营销活动状态：</p><ul><li>将营销活动置为在线要求前置状态为下线</li><li>将营销活动置为下线要求前置状态为在线</li><li>删除没有前置状态要求</li><li>完成自测状态为复访营销活动可设置的状态，需前置完成自测动作</li></ul>
     */
    private Integer status;

    public String getActivity_id() {
        return activity_id;
    }

    public UpdatePromotionActivityStatusV2Query setActivity_id(String activity_id) {
        this.activity_id = activity_id;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public UpdatePromotionActivityStatusV2Query setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public static UpdatePromotionActivityStatusV2QueryBuilder builder() {
        return new UpdatePromotionActivityStatusV2QueryBuilder();
    }

    public static final class UpdatePromotionActivityStatusV2QueryBuilder {
        private String activity_id;
        private Integer status;
        private Integer tenantId;
        private String clientKey;

        private UpdatePromotionActivityStatusV2QueryBuilder() {}

        public UpdatePromotionActivityStatusV2QueryBuilder activityId(String activityId) {
            this.activity_id = activityId;
            return this;
        }

        public UpdatePromotionActivityStatusV2QueryBuilder status(Integer status) {
            this.status = status;
            return this;
        }

        public UpdatePromotionActivityStatusV2QueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public UpdatePromotionActivityStatusV2QueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public UpdatePromotionActivityStatusV2Query build() {
            UpdatePromotionActivityStatusV2Query updatePromotionActivityStatusV2Query = new UpdatePromotionActivityStatusV2Query();
            updatePromotionActivityStatusV2Query.setActivity_id(activity_id);
            updatePromotionActivityStatusV2Query.setStatus(status);
            updatePromotionActivityStatusV2Query.setTenantId(tenantId);
            updatePromotionActivityStatusV2Query.setClientKey(clientKey);
            return updatePromotionActivityStatusV2Query;
        }
    }
}
