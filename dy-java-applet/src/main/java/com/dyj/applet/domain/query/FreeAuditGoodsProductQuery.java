package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * @author danmo
 * @date 2024-05-06 11:07
 **/
public class FreeAuditGoodsProductQuery extends BaseQuery {

    /**
     * 商品id (商品id或商品外部id 选其一）
     */
    private String product_id;

    /**
     * 商品外部id (商品id或商品外部id 选其一）
     */
    private String out_id;
    /**
     * 售卖结束时间
     */
    private Long sold_end_time;
    /**
     * 总库存
     */
    private Long stock_qty;
    /**
     * 商品归属账户ID，非必传；传入时须与该商家满足商服关系
     */
    private String owner_account_id;

    public static FreeAuditGoodsProductQueryBuilder build() {
        return new FreeAuditGoodsProductQueryBuilder();
    }

    public static class FreeAuditGoodsProductQueryBuilder {
        private String productId;
        private String outId;
        private Long soldEndTime;
        private Long stockQty;
        private String ownerAccountId;
        private Integer tenantId;
        private String clientKey;

        public FreeAuditGoodsProductQueryBuilder productId(String productId) {
            this.productId = productId;
            return this;
        }
        public FreeAuditGoodsProductQueryBuilder outId(String outId) {
            this.outId = outId;
            return this;
        }
        public FreeAuditGoodsProductQueryBuilder soldEndTime(Long soldEndTime) {
            this.soldEndTime = soldEndTime;
            return this;
        }
        public FreeAuditGoodsProductQueryBuilder stockQty(Long stockQty) {
            this.stockQty = stockQty;
            return this;
        }
        public FreeAuditGoodsProductQueryBuilder ownerAccountId(String ownerAccountId) {
            this.ownerAccountId = ownerAccountId;
            return this;
        }
        public FreeAuditGoodsProductQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }
        public FreeAuditGoodsProductQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }
        public FreeAuditGoodsProductQuery build() {
            FreeAuditGoodsProductQuery freeAuditGoodsProductQuery = new FreeAuditGoodsProductQuery();
            freeAuditGoodsProductQuery.setProduct_id(productId);
            freeAuditGoodsProductQuery.setOut_id(outId);
            freeAuditGoodsProductQuery.setSold_end_time(soldEndTime);
            freeAuditGoodsProductQuery.setStock_qty(stockQty);
            freeAuditGoodsProductQuery.setOwner_account_id(ownerAccountId);
            freeAuditGoodsProductQuery.setTenantId(tenantId);
            freeAuditGoodsProductQuery.setClientKey(clientKey);
            return freeAuditGoodsProductQuery;
        }
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }

    public Long getSold_end_time() {
        return sold_end_time;
    }

    public void setSold_end_time(Long sold_end_time) {
        this.sold_end_time = sold_end_time;
    }

    public Long getStock_qty() {
        return stock_qty;
    }

    public void setStock_qty(Long stock_qty) {
        this.stock_qty = stock_qty;
    }

    public String getOwner_account_id() {
        return owner_account_id;
    }

    public void setOwner_account_id(String owner_account_id) {
        this.owner_account_id = owner_account_id;
    }
}
