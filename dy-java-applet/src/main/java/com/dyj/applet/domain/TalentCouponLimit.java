package com.dyj.applet.domain;

/**
 * 主播发券配置信息
 */
public class TalentCouponLimit {


    /**
     * <p>账号类型，1：talent_account，2：open_id，与请求参数一致</p>
     */
    private Integer account_type;
    /**
     * <p>小程序appid</p>
     */
    private String app_id;
    /**
     * <p>奖励券授权张数，仅分享裂变时会返回</p> 选填
     */
    private Long award_assigned_num;
    /**
     * <p>抖音开平券模板id</p>
     */
    private String coupon_meta_id;
    /**
     * <p>用户open_id，account_type为2时填充该值，与请求参数一致</p> 选填
     */
    private String open_id;
    /**
     * <p>券的<strong>发放场景</strong>，默认1：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">取值1：直播间内发放</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">取值2：接口发放</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">取值3：平台活动发放</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">取值4：IM场景发放</li></ul> 选填
     */
    private Integer send_scene;
    /**
     * <p>当前主播的发券状态，1：上架（生效），2：下架（失效）</p>
     */
    private Integer status;
    /**
     * <p>主播对于某个券模板的库存发放上限</p>
     */
    private Long stock_limit;
    /**
     * <p>主播抖音号，account_type为1时填充该值，与请求参数一致</p> 选填
     */
    private String talent_account;

    public Integer getAccount_type() {
        return account_type;
    }

    public TalentCouponLimit setAccount_type(Integer account_type) {
        this.account_type = account_type;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public TalentCouponLimit setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public Long getAward_assigned_num() {
        return award_assigned_num;
    }

    public TalentCouponLimit setAward_assigned_num(Long award_assigned_num) {
        this.award_assigned_num = award_assigned_num;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public TalentCouponLimit setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public String getOpen_id() {
        return open_id;
    }

    public TalentCouponLimit setOpen_id(String open_id) {
        this.open_id = open_id;
        return this;
    }

    public Integer getSend_scene() {
        return send_scene;
    }

    public TalentCouponLimit setSend_scene(Integer send_scene) {
        this.send_scene = send_scene;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public TalentCouponLimit setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public Long getStock_limit() {
        return stock_limit;
    }

    public TalentCouponLimit setStock_limit(Long stock_limit) {
        this.stock_limit = stock_limit;
        return this;
    }

    public String getTalent_account() {
        return talent_account;
    }

    public TalentCouponLimit setTalent_account(String talent_account) {
        this.talent_account = talent_account;
        return this;
    }
}
