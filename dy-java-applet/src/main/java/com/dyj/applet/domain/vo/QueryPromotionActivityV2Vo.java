package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.QueryPromotionActivityV2;

/**
 * 查询营销活动返回值
 */
public class QueryPromotionActivityV2Vo {


    /**
     * <p>err_no非0时对应的错误信息文案提示，成功时为空字符串</p>
     */
    private String err_msg;
    /**
     * <p>0表示成功</p>
     */
    private Integer err_no;
    /**
     * <p>标识请求的唯一id</p>
     */
    private String log_id;
    /**
     *
     */
    private QueryPromotionActivityV2 promotion_activity;

    public String getErr_msg() {
        return err_msg;
    }

    public QueryPromotionActivityV2Vo setErr_msg(String err_msg) {
        this.err_msg = err_msg;
        return this;
    }

    public Integer getErr_no() {
        return err_no;
    }

    public QueryPromotionActivityV2Vo setErr_no(Integer err_no) {
        this.err_no = err_no;
        return this;
    }

    public String getLog_id() {
        return log_id;
    }

    public QueryPromotionActivityV2Vo setLog_id(String log_id) {
        this.log_id = log_id;
        return this;
    }

    public QueryPromotionActivityV2 getPromotion_activity() {
        return promotion_activity;
    }

    public QueryPromotionActivityV2Vo setPromotion_activity(QueryPromotionActivityV2 promotion_activity) {
        this.promotion_activity = promotion_activity;
        return this;
    }
}
